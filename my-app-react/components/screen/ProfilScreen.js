import React from 'react';
import { Component } from 'react';
import { ButtonStylized } from '../component/Button/Button';
import {
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';

import Recap from '../Recap/RecapCarsScreen';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUser, fetchUserPosts } from '../../redux/actions/index';
export class Profil extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: null
    }
  }

  componentDidMount() {
    this.props.fetchUser();
    this.props.fetchUserPosts();
  }

  render() {
    const {currentUser, posts} = this.props
    return (
      <View style={styles.container}>
        <View style={styles.profil}>

          <View style={styles.header}>
            <View style={styles.headerContent}>
              <Image style={styles.avatar} source={{ uri: 'https://bootdey.com/img/Content/avatar/avatar2.png' }} />
              {currentUser && <Text style={styles.name}>{currentUser.name}</Text>}
            </View>
          </View>

          <View style={styles.profileDetail}>
            <View style={styles.detailContent}>
              <Text style={styles.title}>Revenus</Text>
              <Text style={styles.countR}>200</Text>
            </View>
            <View style={styles.detailContent}>
              <Text style={styles.title}>Dépenses</Text>
              <Text style={styles.countD}>200</Text>
            </View>
            <View style={styles.detailContent}>
              <Text style={styles.title}>Bénéfices</Text>
              <Text style={styles.countB}>200</Text>
            </View>
          </View>

          <View style={styles.bodyContent}>

            <ButtonStylized
              label='&#10010; car'
              onPress={() => this.props.navigation.navigate('Voiture')}
            //changeState={ value => setValue(value)}
            //state={this.state.buttonState}
            //onPress={this.toggle}
            // onPress={this.setState({ buttonState: 'secondary' })}
            //onPress={() => this.props.navigation.navigate('Gallery')} 
            />
            <ButtonStylized
              label='Button'
            //onPress={() => this.props.navigation.navigate('')} 
            />

          </View>
        </View>

        <Recap style={styles.recap} navigation={this.props.navigation} ></Recap>

      </View>
    );
  }
}


const mapStateToProps = (store) => ({
  currentUser: store.userState.currentUser,
  posts: store.userState.posts
})
const mapDispatchProps = (dispatch) => bindActionCreators({ fetchUser, fetchUserPosts }, dispatch);
export default connect(mapStateToProps, mapDispatchProps)(Profil);


const styles = StyleSheet.create({
  /****** disposition *********************/
  profil: {},
  recap: {

  },
  /****** content *********************/
  header: {
    backgroundColor: "#FFFFFF",
  },
  headerContent: {
    padding: 30,
    alignItems: 'flex-start',
  },
  avatar: {
    width: 90,
    height: 90,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#124369",
    fontWeight: '600',
  },
  profileDetail: {
    alignSelf: 'center',
    marginTop: 0,
    alignItems: 'center',
    flexDirection: 'column',
    position: 'absolute',
    backgroundColor: "#ffffff"
  },
  detailContent: {
    margin: 10,
    marginLeft: 50,
    alignItems: 'center'
  },
  title: {
    fontSize: 14,
    color: "#124369"
  },
  countR: {
    color: 'green',
    fontSize: 18,
  },
  countD: {
    color: 'red',
    fontSize: 18,
  },
  countB: {
    color: '#124369',
    fontSize: 18,
  },
  bodyContent: {
    alignSelf: 'flex-end',
    marginTop: 0,
    position: 'absolute',
    alignItems: 'center',
    padding: 0,
    zIndex: 2,
  },
  textInfo: {
    fontSize: 18,
    marginTop: 20,
    color: "#696969",
  },
  buttonContainer: {
    marginTop: 30,
    marginRight: 30,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 45,
    borderRadius: 30,
    backgroundColor: "#FFFFFF",
  },
  description: {
    fontSize: 20,
    color: "#00CED1",
    marginTop: 10,
    textAlign: 'center'
  },
});
