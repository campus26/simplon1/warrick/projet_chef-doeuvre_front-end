import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Constant } from '../../Constant';
import { ButtonStylized } from '../component/Button/Button';

class Carrosserie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      facad_av: '',
      facad_ar: '',
      facad_left: '',
      facad_right: '',
      toit: '',
      idCarro: [],
    }
  }

  componentDidMount() {
    const onIndexCarrosserie = () => {
      const { itemId } = this.props.route.params;
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");


      var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch(Constant.urlCarrosserieIndex, requestOptions)
        .then(response => response.json())
        .then(data => {
          const idCarro = data.map(obj => {
            if (obj.cleVoiture.id === itemId)
              return obj.id
          })
          .sort();
          console.log(idCarro);
          this.setState({ idCarro });
        })
        .catch(error => console.log('error', error));
    };
    onIndexCarrosserie();
  }

  // onCreateCarrosserie = (itemId) => {
  //   const { facad_av, facad_ar, facad_left, facad_right, toit } = this.state;

  //   var myHeaders = new Headers();
  //   myHeaders.append("Content-Type", "application/json");

  //   var raw = JSON.stringify({
  //     "cle_voiture": itemId,
  //     "facad_av": facad_av,
  //     "facad_ar": facad_ar,
  //     "facad_left": facad_left,
  //     "facad_right": facad_right,
  //     "toit": toit
  //   });

  //   var requestOptions = {
  //     method: 'POST',
  //     headers: myHeaders,
  //     body: raw,
  //     redirect: 'follow'
  //   };

  //   fetch(Constant.urlCarrosserieNew, requestOptions)
  //     .then(response => response.text())
  //     .then(result => console.log('FETCH-Create-Carrosserie:' + result))
  //     .catch(error => console.log('error', error));

  //   this.props.navigation.navigate('Profile');
  // }

  render() {
    const { itemId } = this.props.route.params;
    console.log('return:',  this.state.idCarro );
    return (
      <ScrollView>
        <View style={styles.container}>
          <ButtonStylized label='facade avant' onPress={() => this.props.navigation.navigate('Camera', { input: 'facad_av', itemId: itemId, idCarro: this.state.idCarro })} />
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
              placeholder="Façade avant"
              keyboardType="name"
              underlineColorAndroid='transparent'
              onChangeText={(facad_av) => this.setState({ facad_av })} />
            <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
          </View>
          <ButtonStylized label='facade arriere' onPress={() => this.props.navigation.navigate('Camera', { input: "facad_ar", itemId: itemId, idCarro: this.state.idCarro })} />
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
              placeholder="Façade arrière"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(facad_ar) => this.setState({ facad_ar })} />
            <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
          </View>
          <ButtonStylized label='facade conducteur' onPress={() => this.props.navigation.navigate('Camera', { input: 'facad_left', itemId: itemId, idCarro: this.state.idCarro })} />
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
              placeholder="Façade conducteur"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(facad_left) => this.setState({ facad_left })} />
            <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
          </View>
          <ButtonStylized label='facade passager' onPress={() => this.props.navigation.navigate('Camera', { input: 'facad_right', itemId: itemId, idCarro: this.state.idCarro })} />
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
              placeholder="Façade passager"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(facad_right) => this.setState({ facad_right })} />
            <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
          </View>
          <ButtonStylized label='toit' onPress={() => this.props.navigation.navigate('Camera', { input: 'toit', itemId: itemId, idCarro: this.state.idCarro })} />
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
              placeholder="Toit"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(toit) => this.setState({ toit })} />
            <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
          </View>
          <TouchableOpacity style={styles.buttonContainer}>
            <Text style={styles.btnText}>suivant</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

export default Carrosserie;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',

    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginRight: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent'
  },
  btnText: {
    color: "white",
    fontWeight: 'bold'
  }
});