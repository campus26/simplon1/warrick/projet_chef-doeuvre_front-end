import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Constant } from '../../Constant';

class Papier extends Component {
  constructor(props) {
    super(props);
    this.state = {
        assurance_voiture: '',
        carte_grise: '',
        controle_tech: '',
        permis_conduire: '',
    }
    this.onCreatePapier = this.onCreatePapier.bind(this)
  }

  onCreatePapier = (itemId) => {
    console.log(itemId)
    const { assurance_voiture, carte_grise, controle_tech, permis_conduire } = this.state;

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      "cle_voiture": itemId,
      "assurance_voiture": assurance_voiture,
      "carte_grise": carte_grise,
      "controle_tech": controle_tech,
      "permis_conduire": permis_conduire
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch(Constant.urlPapierNew, requestOptions)
      .then(response => response.text())
      .then(result => console.log('FETCH-Create-Papier:' + result))
      .catch(error => console.log('error', error));

    this.props.navigation.navigate('Profile');
  }

  render() {
    const { itemId } = this.props.route.params;
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="Assurance voiture"
            keyboardType="name"
            underlineColorAndroid='transparent'
            onChangeText={(assurance_voiture) => this.setState({ assurance_voiture })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="Carte grise"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            onChangeText={(carte_grise) => this.setState({ carte_grise })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="Controle technique"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            onChangeText={(controle_tech) => this.setState({ controle_tech })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="Permis de conduire"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            onChangeText={(permis_conduire) => this.setState({ permis_conduire })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
        </View>
        <TouchableOpacity style={styles.buttonContainer} onPress={() => this.onCreatePapier(itemId)}>
          <Text style={styles.btnText}>suivant</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Papier;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',

    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginRight: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent'
  },
  btnText: {
    color: "white",
    fontWeight: 'bold'
  }
});