import React, { useState } from 'react';
import { View, Text, Image, Button } from 'react-native';

import firebase from 'firebase';
require("firebase/firestore")
require("firebase/firebase-storage")

import { Constant } from '../../Constant';

export function SaveScreen(props, { navigation }) {

    console.log(props.route.params)
    const input = props.route.params.input;
    const itemId = props.route.params.itemId; // ID Voiture
    const idCarro = props.route.params.idCarro; // ID Carrosserie de la voiture
    console.log('idCarro:', idCarro);

    const [caption, setCaption] = useState(input)

    const uploadImage = async () => {

        const uri = props.route.params.image;
        const childPath = `post/${firebase.auth().currentUser.uid}/${Math.random().toString(36)}`;
        console.log(childPath)

        const response = await fetch(uri);
        const blob = await response.blob();

        const task = firebase
            .storage()
            .ref()
            .child(childPath)
            .put(blob);

        const taskProgress = snapshot => {
            console.log(`transferred: ${snapshot.bytesTransferred}`)
        };

        const taskCompleted = () => {
            task.snapshot.ref.getDownloadURL().then((snapshot) => {
                savePostData(snapshot);
                console.log('TaskCompleted:', snapshot)
                { idCarro[0] === undefined ? onCreateCarrosserie(snapshot) : onEditCarrosserie(snapshot) }
            })
        }

        const taskError = snapshot => {
            console.log(snapshot)
        }

        task.on("state_changed", taskProgress, taskError, taskCompleted);

    }

    const savePostData = (downloadURL) => {
        firebase.firestore()
            .collection('posts')
            .doc(firebase.auth().currentUser.uid)
            .collection("userposts")
            .add({
                itemId,
                downloadURL,
                caption,
                creation: firebase.firestore.FieldValue.serverTimestamp()
            }).then((function () {
                props.navigation.popToTop()
            }))
    }

    const onCreateCarrosserie = (snapshot) => {
        const input = props.route.params.input;
        const itemId = props.route.params.itemId;

        console.log('Carro Symfony:', itemId, input, snapshot)

        var rawAv = JSON.stringify({ "facad_av": snapshot, "cle_voiture": itemId, });
        var rawAr = JSON.stringify({ "facad_ar": snapshot, "cle_voiture": itemId, });
        var rawLeft = JSON.stringify({ "facad_left": snapshot, "cle_voiture": itemId, });
        var rawRight = JSON.stringify({ "facad_right": snapshot, "cle_voiture": itemId, });
        var rawToit = JSON.stringify({ "toit": snapshot, "cle_voiture": itemId, });

        if (input === "facad_av") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawAv)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawAv,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieNew, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Create-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
        if (input === "facad_ar") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawAr)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawAr,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieNew, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Create-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
        if (input === "facad_left") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawLeft)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawLeft,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieNew, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Create-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
        if (input === "facad_right") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawRight)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawRight,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieNew, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Create-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
        if (input === "toit") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawToit)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawToit,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieNew, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Create-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
    }

    const onEditCarrosserie = (snapshot) => {
        const input = props.route.params.input;
        const itemId = props.route.params.itemId;

        console.log('Carro Symfony:', itemId, input, snapshot)

        var rawAv = JSON.stringify({ "facad_av": snapshot, });
        var rawAr = JSON.stringify({ "facad_ar": snapshot, });
        var rawLeft = JSON.stringify({ "facad_left": snapshot, });
        var rawRight = JSON.stringify({ "facad_right": snapshot, });
        var rawToit = JSON.stringify({ "toit": snapshot, });

        if (input === "facad_av") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawAv)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawAv,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieEdit + itemId, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Edit-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
        if (input === "facad_ar") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawAr)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawAr,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieEdit + itemId, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Edit-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
        if (input === "facad_left") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawLeft)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawLeft,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieEdit + itemId, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Edit-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
        if (input === "facad_right") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawRight)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawRight,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieEdit + itemId, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Edit-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
        if (input === "toit") {

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            console.log('RAW:', rawToit)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rawToit,
                redirect: 'follow'
            };

            fetch(Constant.urlCarrosserieEdit + itemId, requestOptions)
                .then(response => response.text())
                .then(result => console.log('FETCH-Edit-Carrosserie:' + result))
                .catch(error => console.log('error', error));
        }
    }


    return (
        <View style={{ flex: 1 }}>
            <Image source={{ uri: props.route.params.image }} />
            {input && <Text> {input} </Text>}
            {itemId && <Text> id voiture {itemId} </Text>}
            <Button title='Save' onPress={() => uploadImage()} />
        </View>
    )
}