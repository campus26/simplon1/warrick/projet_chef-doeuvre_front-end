import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import Api from '../ApiScreen';

class Recap extends Api {

  render() {
    return (

      <FlatList
        style={styles.list}
        data={this.state.bdd}
        keyExtractor={(item) => {
          return item.id;
        }}
        ItemSeparatorComponent={() => {
          return (
            <View style={styles.separator} />
          )
        }}
        renderItem={(post) => {
          const item = post.item;
          const itemId = item.id;
          return (
            <View style={styles.card}>
              <TouchableOpacity style={styles.socialBarButton} onPress={() => this.props.navigation.navigate('Detail', { itemId: itemId })}>
                <Image style={styles.cardImage} source={{ uri: "https://i.gaw.to/content/photos/41/05/410584_Ford_Mustang.jpg" }} />
                <View style={styles.cardHeader}>
                  <View style={styles.cardContent}>
                    <View style={styles.cardText}>
                      <Text style={styles.matricule}>{item.matricule}</Text>
                      <Text style={styles.puissance}>{item.cheveaux} ch</Text>
                      <Text style={styles.adresse}>{item.adresse}</Text>
                    </View>
                    <View>
                      <Text style={styles.type}>{item.carburan_type}</Text>
                      <Text style={styles.description}>{item.prix} €/mois</Text>
                      <View style={styles.timeContainer}>
                        <Image style={styles.iconData} source={{ uri: 'https://img.icons8.com/color/96/3498db/calendar.png' }} />
                        <Text style={styles.time}>{item.time}</Text>
                        <Image style={styles.icon} source={{ uri: 'https://img.icons8.com/nolan/96/3498db/add-shopping-cart.png' }} />
                        <Text style={styles.detail}>Détail</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )
        }} />
    );
  }
}

export default Recap;

const styles = StyleSheet.create({
  list: {
    backgroundColor: "#E6E6E6",
    height: '72%',
  },
  separator: {
    marginTop: 10,
  },
  /******** card **************/
  card: {
    shadowColor: "#124369",
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,
    elevation: 22,
    marginVertical: 30,
    marginHorizontal: 30,
    backgroundColor: "#124369",
    borderRadius: 20,
  },
  cardHeader: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 15,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  cardContent: {
    width: '100%',
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardText: {
    margin: 2,
  },
  cardImage: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    height: 200,
    width: '100%',
  },
  /******** card components **************/
  matricule: {
    color: '#124369',
    fontSize: 18,
    flex: 1,
  },
  type: {
    color: '#124369',
  },
  puissance: {
    color: '#124369',
  },
  adresse: {
    color: '#124369',
    height: 'auto',
    maxWidth: 150,
  },
  description: {
    fontSize: 15,
    color: "#888",
    flex: 1,
    marginTop: 5,
    marginBottom: 5,
  },
  time: {
    fontSize: 13,
    color: "#808080",
    marginTop: 5
  },
  icon: {
    width: 25,
    height: 25,
  },
  iconData: {
    width: 15,
    height: 15,
    marginTop: 5,
    marginRight: 5
  },
  timeContainer: {
    flexDirection: 'row'
  },
  /******** social bar ******************/
  socialBarContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1
  },
  socialBarSection: {
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  detail: {
    marginLeft: 8,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    color: '#124369',
  },
  socialBarButton: {
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

  }
});