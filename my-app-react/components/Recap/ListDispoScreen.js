import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    FlatList,
    SafeAreaView,
    TextInput,
} from 'react-native';
import { Constant } from '../../Constant';

const ListDispo = ({ navigation, log1, log2 }) => {

    const [search, setSearch] = useState('');
    const [filteredDataSource, setFilteredDataSource] = useState([]);
    const [masterDataSource, setMasterDataSource] = useState([]);

    useEffect(() => {
        fetch(Constant.urlVoitureIndex)
            .then((response) => response.json())
            .then((responseJson) => {
                setFilteredDataSource(responseJson);
                setMasterDataSource(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });
    }, []);

    const searchFilterFunction = (text) => {
        // Check if searched text is not blank
        if (text) {
            // Inserted text is not blank
            // Filter the masterDataSource
            // Update FilteredDataSource
            const newData = masterDataSource.filter(
                function (item) {
                    const itemData = item.matricule
                        ? item.matricule.toUpperCase()
                        : ''.toUpperCase();
                    const textData = text.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                });
            setFilteredDataSource(newData);
            setSearch(text);
        } else {
            // Inserted text is blank
            // Update FilteredDataSource with masterDataSource
            setFilteredDataSource(masterDataSource);
            setSearch(text);
        }
    };

    const ItemView = ({ item }) => {
        const itemId = item.id;
        return (
            // Flat List Item
            <View style={styles.card}>
               {!log1 || !log2 ? <TouchableOpacity style={styles.socialBarButton} onPress={() => navigation.navigate('Register')}> 
                    <Image style={styles.cardImage} source={{ uri: "https://i.gaw.to/content/photos/41/05/410584_Ford_Mustang.jpg" }} />
                    <View style={styles.cardHeader}>
                        <View style={styles.cardContent}>
                            <View style={styles.cardText}>
                                <Text style={styles.matricule}>{item.matricule}</Text>
                                <Text style={styles.puissance}>{item.cheveaux} ch</Text>
                                <Text style={styles.adresse}>{item.adresse}</Text>
                            </View>
                            <View>
                                <Text style={styles.type}>{item.carburan_type}</Text>
                                <Text style={styles.description}>{item.prix} €/mois</Text>
                                <View style={styles.timeContainer}>
                                    <Image style={styles.iconData} source={{ uri: 'https://img.icons8.com/color/96/3498db/calendar.png' }} />
                                    <Text style={styles.time}>{item.time}</Text>
                                    <Image style={styles.icon} source={{ uri: 'https://img.icons8.com/nolan/96/3498db/add-shopping-cart.png' }} />
                                    <Text style={styles.detail}>Détail</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                 : 
                 <TouchableOpacity style={styles.socialBarButton} onPress={() => navigation.navigate('Detail', { itemId: itemId })}> 
                    <Image style={styles.cardImage} source={{ uri: "https://i.gaw.to/content/photos/41/05/410584_Ford_Mustang.jpg" }} />
                    <View style={styles.cardHeader}>
                        <View style={styles.cardContent}>
                            <View style={styles.cardText}>
                                <Text style={styles.matricule}>{item.matricule}</Text>
                                <Text style={styles.puissance}>{item.cheveaux} ch</Text>
                                <Text style={styles.adresse}>{item.adresse}</Text>
                            </View>
                            <View>
                                <Text style={styles.type}>{item.carburan_type}</Text>
                                <Text style={styles.description}>{item.prix} €/mois</Text>
                                <View style={styles.timeContainer}>
                                    <Image style={styles.iconData} source={{ uri: 'https://img.icons8.com/color/96/3498db/calendar.png' }} />
                                    <Text style={styles.time}>{item.time}</Text>
                                    <Image style={styles.icon} source={{ uri: 'https://img.icons8.com/nolan/96/3498db/add-shopping-cart.png' }} />
                                    <Text style={styles.detail}>Détail</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>}
            </View>
        );
    };

    const ItemSeparatorView = () => {
        return (
            // Flat List Item Separator
            <View
                style={styles.separator}
            />
        );
    };

    const getItem = (item) => {
        // Function for click on an item
        alert('Id : ' + item.id + ' Title : ' + item.title);
    };

    return (
        <View>

            <View style={styles.container}>
                <TextInput
                    style={styles.textInputStyle}
                    onChangeText={(text) => searchFilterFunction(text)}
                    value={search}
                    underlineColorAndroid="transparent"
                    placeholder="Search Here"
                />
                <FlatList
                    style={styles.list}
                    data={filteredDataSource}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeparatorView}
                    renderItem={ItemView}
                />
            </View>

        </View>
    );
}


export default ListDispo;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
    },
    itemStyle: {
        padding: 10,
    },
    textInputStyle: {
        height: 40,
        borderWidth: 2,
        paddingLeft: 20,
        margin: 5,
        color: '#124369',
        borderColor: '#124369',
        backgroundColor: '#FFFFFF',
    },
    list: {
        backgroundColor: "#E6E6E6",
        height: '95%',
    },
    separator: {
        marginTop: 10,
    },
    /******** card **************/
    card: {
        shadowColor: "#124369",
        shadowOffset: {
            width: 0,
            height: 11,
        },
        shadowOpacity: 0.55,
        shadowRadius: 14.78,
        elevation: 22,
        marginVertical: 30,
        marginHorizontal: 30,
        backgroundColor: "#124369",
        borderRadius: 20,
    },
    cardHeader: {
        backgroundColor: '#FFFFFF',
        paddingVertical: 15,
        borderTopLeftRadius: 1,
        borderTopRightRadius: 1,
        flexDirection: 'row',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    cardContent: {
        width: '100%',
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    cardText: {
        margin: 2,
    },
    cardImage: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        height: 200,
        width: '100%',
    },
    /******** card components **************/
    matricule: {
        color: '#124369',
        fontSize: 18,
        flex: 1,
    },
    type: {
        color: '#124369',
    },
    puissance: {
        color: '#124369',
    },
    adresse: {
        color: '#124369',
        height: 'auto',
        maxWidth: 150,
    },
    description: {
        fontSize: 15,
        color: "#888",
        flex: 1,
        marginTop: 5,
        marginBottom: 5,
    },
    time: {
        fontSize: 13,
        color: "#808080",
        marginTop: 5
    },
    icon: {
        width: 25,
        height: 25,
    },
    iconData: {
        width: 15,
        height: 15,
        marginTop: 5,
        marginRight: 5
    },
    timeContainer: {
        flexDirection: 'row'
    },
    /******** social bar ******************/
    socialBarContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1
    },
    socialBarSection: {
        justifyContent: 'center',
        flexDirection: 'row',
        flex: 1,
    },
    detail: {
        marginLeft: 8,
        alignSelf: 'flex-end',
        justifyContent: 'center',
        color: '#124369',
    },
    socialBarButton: {
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    }
});