import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    FlatList,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUserPosts } from '../../redux/actions/index';

export class RecapCarrosserieScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: null,
        }
    }

    componentDidMount() {
        this.props.fetchUserPosts();
    }

    render() {
        const { posts } = this.props;
        console.log('RECAP-CARROSSERIE', posts)
        return (
            <ScrollView>
                {posts &&
                    <FlatList style={styles.list}
                        numColumns={3}
                        horizontal={false}
                        data={posts}
                        keyExtractor={(item) => {
                            return item.itemId;
                        }}
                        renderItem={(post) => {
                            const item = post.item;
                            if (item.itemId === this.props.itemId)
                                return (
                                    <Image style={styles.img_carrosserie} source={{ uri: item.downloadURL }} />
                                )
                        }} />
                }
            </ScrollView>
        )
    }
}

const mapStateToProps = (store) => ({
    posts: store.userState.posts
});
const mapDispatchProps = (dispatch) => bindActionCreators({ fetchUserPosts }, dispatch);
export default connect(mapStateToProps, mapDispatchProps)(RecapCarrosserieScreen);

const styles = StyleSheet.create({
    list: {
        backgroundColor: "#E6E6E6",
        height: '40%',
        margin: 20,
    },
    
    img_carrosserie: {
        flex: 1,
        aspectRatio: 1 / 1,
        margin: 10,
    }
});