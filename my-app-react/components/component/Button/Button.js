import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {StyleSheet,Text,} from 'react-native';
import { Button } from 'react-native-paper';

/**
 * Primary UI component for user interaction
 */
export const ButtonStylized = ({ state, label, onClick, imgSide, Icon, ...props }) => {
  // const mode = 'button_' + state;
  // const iconmode = 'button__icon--' + imgSide;

  const [etat, setEtat] = useState('button_' + state);

  const changeState = () => {
    if (etat === 'button_primary'){
      setEtat('button_secondary');
    } else {
      setEtat('button_primary')
    }
  }

  return (
    <Button
      style={[styles.button, styles[etat]]}
      onPress={changeState}
      {...props}
    >



        {label && <Text style={styles[etat]}>{label}</Text>}



    </Button>
  );
};

ButtonStylized.propTypes = {
  /**
   * variant of the button used
   */
  state: PropTypes.oneOf(['primary', 'secondary', 'tertiary', 'disabled']).isRequired,
  /**
   * Button contents
   */
  label: PropTypes.string,
  /**
   * The icon is right or left side of label, imgSide = 'right' || 'left'
   */
  imgSide: PropTypes.string,
  /**
   * Icon du button si left || right
   */
  Icon: PropTypes.func,
  /**
   * Optional click handler
   */
  onClick: PropTypes.func,
};

ButtonStylized.defaultProps = {
  state: 'primary',
  imgSide: undefined,
  onClick: undefined,
  Icon: undefined
};

const styles = StyleSheet.create({
  button: {
    fontFamily: "'Roboto', 'Helvetica Neue', Helvetica, Arial, sans- serif",
    fontWeight: "700",
    fontSize: 5,
    lineHeight: 16,
    textTransform: 'uppercase',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#124369',
    //border: '1px solid hsl(206, 71 %, 24 %)',
    borderRadius: 10,
    color: '#124369',
    margin: 10,
    padding: 4,
    paddingLeft: 6,
    paddingRight: 6,
    //boxShadow: '0px 2px 0px #124369',
    shadowColor: "#124369",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.95,
    shadowRadius: 4.78,
    elevation: 12,
    minWidth: 77,
  },

  button_primary: {
    textAlign: 'center',
    color: '#FFFFFF',
    backgroundColor: '#124369',
  },
  button_secondary: {
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    color: '#124369',
  },
  button_tertiary: {
    borderColor: 'transparent',
    shadowOpacity: 0,
    backgroundColor: '#FFFFFF',
  },
  button_disabled: {
    borderColor: 'transparent',
    shadowOpacity: 0,
    backgroundColor: '#EAEDF1',
    color: '#9EADBD',
  },

  button__icon: {
    position: 'relative',
    top: '4px',
    width: '16px',
    height: '16px',
    color: 'white',
  },

  button_right: {
    marginLeft: '10px',
    marginRight: '0px',
  },

  button_left: {
    marginLeft: '0px',
    marginRight: '10px',
  },

  button_none: {
    display: 'none',
  },


  button__content: {

  }
})