import { Component } from 'react';
import * as React from 'react';
import { View, Text } from 'react-native';

import firebase from 'firebase';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './redux/reducers';
import thunk from 'redux-thunk';

const store = createStore(rootReducer, applyMiddleware(thunk));

const firebaseConfig = {
  apiKey: "AIzaSyDEr4mrNR5LA_zWorZQ7MXlriAZpp1FzFw",
  authDomain: "applocation-12d51.firebaseapp.com",
  projectId: "applocation-12d51",
  storageBucket: "applocation-12d51.appspot.com",
  messagingSenderId: "102820340991",
  appId: "1:102820340991:web:917ec5e0afb255c58cd8ca",
  measurementId: "G-HNEHY3BDWV"
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig)
};

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AsyncStorage from '@react-native-async-storage/async-storage';

import MainScreen from './components/Main';

import { HomeScreen } from './components/auth/HomeScreen';
import { RegisterScreen } from './components/auth/RegisterScreen';
import { LoginScreen } from './components/auth/LoginScreen';
import Carrosserie from './components/Form/FormCarrosserieScreen';
import Papier from './components/Form/FormPapiersScreen';
import { AddCarsScreen } from './components/Form/AddCarsScreen';
import { DetailScreen } from './components/screen/DetailScreen';
import { CameraScreen } from './components/src/CameraScreen';
import { GalerieScreen } from './components/src/GalerieScreen';
import { SaveScreen } from './components/src/SaveScreen';
import RecapCarrosserieScreen from './components/Recap/RecapCarrosserieScreen';
import ListDispo from './components/Recap/ListDispoScreen';


const Stack = createStackNavigator();

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadedFirebase: false,
      loadedSymfony: false,
    }
    const useStorage = async () => {
      const value = await AsyncStorage.getItem('token');
      if (value !== null) {
        console.log('HELLO')
        console.log('SYMFONY', value)
        this.setState({
          loggedInSymfony: true,
          loadedSymfony: true,
        })
      } else {
        this.setState({
          loggedInSymfony: false,
          loadedSymfony: true,
        })
      }
    }
    this.useStorage = () => useStorage();
  }

  componentDidMount() {

    this.useStorage();
    ///////////////////////////////////////////////////////
    firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.setState({
          loggedInFirebase: false,
          loadedFirebase: true,
        })
      } else {
        console.log('FIREBASE', user.uid)
        this.setState({
          loggedInFirebase: true,
          loadedFirebase: true,
        })
      }
    })

  }

  render() {
    const { loggedInSymfony, loadedSymfony, loggedInFirebase, loadedFirebase } = this.state;
    if (!loadedSymfony || !loadedFirebase) {
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text>Loading</Text>
        </View>
      )
    }
    if (!loggedInSymfony || !loggedInFirebase) {
      return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="Register" component={RegisterScreen} />
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="ListDispo" log1={loggedInFirebase} log2={loggedInSymfony} component={ListDispo} navigation={this.props.navigation} />
            <Stack.Screen name="Detail" component={DetailScreen} navigation={this.props.navigation} />
            <Stack.Screen name="RecapCarrosserie" component={RecapCarrosserieScreen} navigation={this.props.navigation} />
          </Stack.Navigator>
        </NavigationContainer>
      )
    }
    //  store.dispatch(fetchUserPosts);
    return (
      <Provider store={store}  >
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Main" component={MainScreen} />
            <Stack.Screen name="ListDispo" component={ListDispo} navigation={this.props.navigation} />
            <Stack.Screen name="Detail" component={DetailScreen} navigation={this.props.navigation} />
            <Stack.Screen name="Voiture" component={AddCarsScreen} navigation={this.props.navigation} />
            <Stack.Screen name="Carrosserie" component={Carrosserie} navigation={this.props.navigation} />
            <Stack.Screen name="RecapCarrosserie" component={RecapCarrosserieScreen} navigation={this.props.navigation} />
            <Stack.Screen name="Papier" component={Papier} navigation={this.props.navigation} />
            <Stack.Screen name="Camera" component={CameraScreen} navigation={this.props.navigation} />
            <Stack.Screen name="Galerie" component={GalerieScreen} />
            <Stack.Screen name="Save" component={SaveScreen} navigation={this.props.navigation} />
            <Stack.Screen name="Register" component={RegisterScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}

export default App;


